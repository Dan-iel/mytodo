<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TODO List</title>
    <link href="style.css" rel="stylesheet">
</head>

<body>
    <h1>Users List</h1>

    <div class="container">
        <?php

        // On pré-rempli le formulaire
        require 'connexion.php';
        if (isset($_GET['id'])) {
            $id = (int) $_GET['id'];
            $res = $mysqli->query("SELECT firstName, lastName FROM person WHERE id = " . $id);
            $row = $res->fetch_assoc();
            if (isset($row['firstName']) && isset($row['lastName'])) {
        ?>
        <form method="POST">
            <input type="text" value="<?php echo $row['firstName']; ?>" name="firstName">
            <input type="text" value="<?php echo $row['lastName']; ?>" name="lastName">
            <input type="submit" value="Modifier">
        </form>
        <?php } 
        } ?>

        <?php
        // On met à jour les données
        if (isset($_POST['firstName']) && isset($_POST['lastName'])) {
            $firstNAME = $_POST['firstName'];
            $lastNAME = $_POST['lastName'];
            $requete = "UPDATE person SET firstName='$firstNAME' , lastName='$lastNAME ' WHERE id = '$id'";
            echo($requete);
            $resultat = $mysqli->query($requete);
            if ($resultat) {
                header("Location:index.php");
            } else {
                echo '<p class="error">Une erreur est survenue</p>';
            }
        }
        ?>
    </div>
</body>

</html>