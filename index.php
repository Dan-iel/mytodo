<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TODO List</title>
    <link href="style.css" rel="stylesheet">
</head>

<body>
    <h1>TODO List</h1>

    <div class="container">
        <table>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Task</th>
                <th>Note</th>
                <th class="operation"></th>
                <th class="operation"></th>
            </tr>

            <?php

            require 'connexion.php';
            $tasks = $mysqli->query('SELECT * FROM person 
            JOIN  notes ON person.id = notes.personid
            JOIN taches ON person.id = taches.id;');
           
            foreach ($tasks as $task) { ?>
            <tr>
                <td>
                    <?php echo $task['id'] ?>
                </td>
                <td>
                    <?php echo $task['firstName'] ?>
                </td>
                <td>
                    <?php echo $task['lastName'] ?>
                </td>
                <td>
                    <?php echo $task['task'] ?>
                </td>
               <!-- <td>
                    <?#php echo $task['note'] ?>
                </td>-->
                <td>
                    <a class=" btn" href="edit.php?id=<?php echo $task['id']; ?>">Modifier</a>
                    <a class="btn btn-danger" href="delete.php?id=<?php echo $task['id']; ?>"
                        onclick="return confirm('Êtes vous sûr de vouloir supprimer la tâche: <?php echo $task['task']; ?> ?')">Supprimer</a>
                </td>
            </tr>
            <?php } ?>
        </table>

        <p>
            <a href=" ./assignadder.php" class="btn">
                Ajouter une nouvelle tache et/ou person
            </a>
        </p>

    </div>

</body>

</html>